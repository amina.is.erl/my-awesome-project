package com.example.ttime.repository;

import com.example.ttime.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.UUID;

public interface StatsRepository extends JpaRepository<Invoice, UUID> {

    @Query(nativeQuery = true, value = "" +
            "SELECT SUM(amount) FROM invoice WHERE customer_id = ?1 " +
            "AND issuedAt BETWEEN ?2 and ?3 ")
    public Float getInvoiceAmountByCustomerIdForPeriod(UUID customerId, Instant from, Instant to);

}
