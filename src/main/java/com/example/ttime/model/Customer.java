package com.example.ttime.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;

@Entity
@Data
public class Customer {

    @Id
    @UuidGenerator
    private UUID id;

    private String name;

    private String address;

}
