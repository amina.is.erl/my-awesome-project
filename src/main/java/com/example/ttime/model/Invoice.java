package com.example.ttime.model;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.UuidGenerator;

import java.util.List;
import java.time.Instant;
import java.util.UUID;

@Entity
@Data
public class Invoice {

    @Id
    @UuidGenerator
    private UUID id;

    private String code;

    private String title;

    private String description;

    private Instant issuedAt;

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private Customer customer;

    @OneToMany(mappedBy = "invoice_position")
    private List<InvoicePosition> positions;

    private Float amount;
}
