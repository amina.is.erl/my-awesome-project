package com.example.ttime.controller;

import com.example.ttime.dto.MonthlyStatsDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class MonthlyStatsController {

    public ResponseEntity<MonthlyStatsDTO> getCurrentMonthStatsByCustomerId(@RequestParam("customer_id") UUID customerId) {
        return ResponseEntity.ok().build();
    }
}
