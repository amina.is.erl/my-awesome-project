package com.example.ttime.controller;

import com.example.ttime.dto.InvoiceDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class InvoiceController {

    @PostMapping("/invoice")
    public ResponseEntity create(@RequestBody InvoiceDTO invoiceDTO) {
        return ResponseEntity.ok().build();
    }

    @GetMapping("/invoice/{id}")
    public ResponseEntity get(@PathVariable("id")UUID id) {
        return ResponseEntity.ok().build();
    }
}
