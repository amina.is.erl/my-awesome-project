package com.example.ttime;

import com.example.ttime.dto.MonthlyStatsDTO;
import com.example.ttime.repository.StatsRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAdjuster;
import java.util.UUID;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

@Service
public class StatsService {

    private StatsRepository repository;

    public MonthlyStatsDTO getMonthlyStatsByCustomerId(UUID customerId) {
        Instant startOfTheMonth = LocalDateTime.now().withDayOfMonth(1).toInstant(ZoneOffset.UTC);
        Instant endOfTheMonth = LocalDateTime.now().with(lastDayOfMonth()).toInstant(ZoneOffset.UTC);
        Float amount = repository.getInvoiceAmountByCustomerIdForPeriod(customerId, startOfTheMonth, endOfTheMonth);

    }
}
