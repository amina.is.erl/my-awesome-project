package com.example.ttime.dto;

import lombok.Data;

@Data
public class MonthlyStatsDTO {
    private Float amount;
    private String month;
    private String year;
}
