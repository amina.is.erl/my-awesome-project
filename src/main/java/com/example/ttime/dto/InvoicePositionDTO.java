package com.example.ttime.dto;

import lombok.Data;

@Data
public class InvoicePositionDTO {
    private String description;
    private Float amount;
}
