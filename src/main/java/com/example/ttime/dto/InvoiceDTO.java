package com.example.ttime.dto;

import lombok.Data;

import java.util.UUID;
import java.util.List;

@Data
public class InvoiceDTO {
    private String title;
    private String description;
    private UUID customerId;
    private List<InvoicePositionDTO> positions;
}
